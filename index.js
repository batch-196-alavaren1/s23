
let trainer = {
	
	name: "Misty",
	Age: 15,
	Pokemon: [
		"Charizard",
		"Gengar",
		"Arcanine",
		"Bulbasaur",
	],
	Friends: {
		"Heonn":["May","Max"],
		"Kanto":["Brock","Misty"],
	},

	talk: function(){
		console.log("Pikachu! I choose you!")
	}
};

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer.Pokemon);
console.log("Result of talk method:");
trainer.talk();

function Pokemon(name,level,health,attack) {

	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = attack;

	this.tackle = function(pokemon,pokemon1){

		console.log(this.name + " tackled " + pokemon.name + " " + pokemon1.name);
		pokemon.health = pokemon.health - this.health
		console.log(pokemon.name + "'s health is now reduced to " +  pokemon.health);

	} 
	this.faint = function(){
		console.log(this.name + " has fainted!");

	}

}

let power1 = new Pokemon("Pikachu",12,36,18);
let power2 = new Pokemon("Geudod",8,24,12);
let power3 = new Pokemon("Mewtwo",100,300,150);
console.log(power1);
console.log(power2);
console.log(power3);

power1.tackle(power2,power3);
power2.faint();

console.log(power2);


